//identify the components needed to create the register page.
import { useState, useEffect, useContext } from 'react';
import Hero from '../components/Banner';
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext'; 

const data = {
  title: 'Welcome to the Register Page',
  content: 'Create an Account to Enroll'
}

//declare a state for input fields in teh registration.
//create a side effect on the page to render the button component if teh condition are not yet met.

export default function Register() {

	//destructure form the data from the context object from the user.
	const { user } = useContext(UserContext); // similar in object{}

		//once you passed down the variables into thier respective components thet will bounded to its value.
		//for us to manage the 'states' of our component we will need a 'Hook' in React that will allows us to handle the component.
		//using the acquired Hook from React, we will now applt a 2 way binding from inout elements.
		//syntax: [getter, setter] = useState(value)
		//naming convention fro state setters.
		//set + Variable
		// wrap array structure to contain variables inside a 1 dimensional structure.
		//we will use an evenEmitter taht will allow us to chnage the states of component.
		//why Object Object? => Type  Coersion
		//[] + []
		//{} + [] = type coersion
		//ATM we applied/created a 2 way binding in our form component, this 2 way binding will allow us to create and manage information seamlessly across all our components,
		//check if you succesfully caught the data

		const [firstName, setFirstName] = useState('');
		const [lastName, setlastName] = useState('');
		const [email, setEmail] = useState('');
		const [mobileNo, setMobileNo] = useState('');
		const [password1, setPassword1] = useState('');
		const [password2, setPassword2]  = useState('');

		//declare for the button component, attack a state hook to eventually manage or change to state of the button.
		//to eventually change teh state of the button , insert states setter
		//now  using the state hook, apply a consitional rendering to our button component.
			//is (prefix) = verb
			//=> Yes or nO /True or false
		const [isActive, setIsActive] = useState(false);

		//make a reactive response section for the password using conditional rendering.
		const [isMatched, setIsMatched] = useState(false);
		const [isMobileValid, setIsMobileValid] = useState(false);

		//state for the heading section of the form.
		const [isAllowed, setIsAllowed] = useState(false);

		//we can describe the side effects/effects taht will react on how the user will interact with form/
		//to remove the warning
		useEffect(() => {
			//New Task: Make Reactions Swift:

			//we will modify the control structure to best suit our set of rules bounf by register page. LONG VERSION)

			if (
						//check if mobile is valid
						mobileNo.length === 11
			)
				 //all fields are populated
			{
				//this block of code will run if the 1st set of rulle above is met.
						setIsMobileValid(true);
						if (
						//password checker if matched
								password1 === password2 && password1 !== '' && password2 !== ''
							) {
								setIsMatched(true);
						if (firstName !== '' && lastName !== ''&&email !== '') {
								setIsAllowed(true);
								setIsActive(true);
						} 

						else {
								setIsAllowed(false);
								setIsActive(false);
						}
								
						} else {
								setIsMatched(false);
								setIsAllowed(false);
								setIsActive(false);
						}
				} 
			else if(password1 !== '' && password1 === password2) {
							setIsMatched(true);
						}
			else {
				//Not permitted
					setIsActive(false);
					setIsMatched(false);
					setIsMobileValid(false);
					setIsAllowed(false);
			};
		},[firstName, lastName, email, password1, password2, mobileNo]);
		//the effect Hook 'useeffect()' will use the dependencies list in order to identify the which component to watch out for changes. To avoid continous from submission for every render of each component.

	//catch the 'click' event that will happen on the button component
	//we can make it as async to make sure
	const registerUser = async (eventSubmit) => {
		eventSubmit.preventDefault()

		//we will now modify this function so that we will be able to send a request to our API so that the client will be bale to create his own account.
		//syntax: fetch(url, option)
		//upon sending this request to create an account a promise willbe initialized.

		const isRegistered = await fetch('https://thawing-gorge-69376.herokuapp.com/users/register',{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
    				"firstName": firstName,
    				"lastName": lastName,
    				"email": email,
    				"password": password1,
    				"mobileNo": mobileNo
				})

		}).then(response => response.json()).then(dataNakaJSON => {
				//console.log(dataNakaJSON);
				//create a control structure if success or fail
				//check if the email props has data.
				if (dataNakaJSON.email) {
						return true;
				} else {
						return false;
				}
		})

		//create a control structure that will evaluate the result of the fetch()

		if (isRegistered) {
			//prompt message, clear the components.redirect the user to the login page if succesfull registration
			//Reset the registratiob form
				setFirstName('')
				setlastName('')
				setEmail('')
				setMobileNo('')
				setPassword1('')
				setPassword2('')

				 await Swal.fire({
					icon: 'success',
					title: 'Registration Successful',
					text: 'Thank you for creating an Account'
				});

				//redirect the user to the login page 
				window.location.href = "/login";
				 
		} else {
				//response if the registration has failed
				Swal.fire({
					icon:'eror',
					title: 'Something When Wrong',
					text: 'Try Agian Later!'
				});
		}
		
};


	return(
		user.id
		?
				<Navigate to="/courses" replace={true}/>
		:
		<>
				<Hero bannerData={data}/>
				<Container>
				{/*Form Heading*/}
				{
					isAllowed ? 
							<h1 className="text-center text-success">You May Now Register!</h1>

					:
							<h1 className="text-center">Register Form</h1>

				}
				<h6 className="text-center mt-3 text-secondary">Fill Up the Form Below</h6>
			<Form onSubmit={e => registerUser(e)}>
			   {/*First Name Field*/}
			   <Form.Group>
			   		<Form.Label>First Name: </Form.Label>
			   		<Form.Control type="text" 
			   		   placeholder="Enter your first Name"
			   		   required 
			   		   value={firstName}
			   		   onChange={event => setFirstName(event.target.value)}
			   		
			   		/>
			   </Form.Group>

			   {/*Last Name Field*/}
			   <Form.Group>
			   		<Form.Label>Last Name:</Form.Label>
			   		<Form.Control 
			   			type="text"
			   			placeholder="Enter your Last Name"
			   			required
			   			value={lastName}
			   			onChange={e => setlastName(e.target.value)}
			   		/>
			   </Form.Group>

               {/*Email Address Field*/}
			   <Form.Group>
			   		<Form.Label>Email:</Form.Label>
			   		<Form.Control
			   		   type="email" 
			   		   placeholder="Insert your Email Address"
			   		   required
			   		   value={email}
			   		   onChange={e => setEmail(e.target.value)}
			   		/>
			   </Form.Group>

			   {/*Mobile Number Field*/}
			   {/*Customize this component so that you will get the correct format for the mobile Number*/}
			   <Form.Group>
			   		<Form.Label>Mobile Number:</Form.Label>
			   		<Form.Control 
			   			type="number"
			   			placeholder="Insert your Mobile No."
			   			required
			   			value={mobileNo}
			   			onChange={e => setMobileNo(e.target.value)}
			   		/>
			   		{
			   			isMobileValid ?
			   				<span className="text-success">Mobile No. is Valid!
			   				</span>			 
			   			:
			   				<span className="text-muted">Mobile No. should be 11-digits</span>
			   		}
			   </Form.Group>

			   {/*Password Field*/}
			   <Form.Group>
			   	  <Form.Label>Password:</Form.Label>
			   	  <Form.Control 
			   	  	 type="password"
			   	  	 placeholder="Enter your password"
			   	  	 required
			   	  	 value={password1}
			   	  	 onChange={e => setPassword1(e.target.value)}
			   	  />
			   </Form.Group>

			   {/*Confirm Password Field*/}
			   <Form.Group>
			   	  <Form.Label>Confirm Password:</Form.Label>
			   	  <Form.Control 
			   	  	 type="password"
			   	  	 placeholder="Confirm your password"
			   	  	 required
			   	  	 value={password2}
			   	  	 onChange={e => setPassword2(e.target.value)}
			   	  />
			   	  {
			   	  	isMatched ? 
			   	  		<span className="text-success">Passwords Match!
			   	  		</span>

			   	  	:
			   	 			 <span className="text-danger">Passwords Should Match!
			   			   </span>
			   	  }
			   </Form.Group>

			   {/*Register Button*/}
			   {
			   	isActive ? 
			   		  <Button 
			     			 className="btn-success btn-block"
			      		 type="submit"
			      		 
			    		> 
			     	  Register 
			   	    </Button>
			     	:
			      	<Button 
			       		className="btn-secondary btn-block"
			       		disabled
			        > 
			        Register 
			       </Button>
			    }

			</Form>
				</Container>
	  </>
	);
};
