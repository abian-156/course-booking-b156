import Hero from './../components/Banner';
import {Container, Form, Button} from 'react-bootstrap';
import Swal from 'sweetalert2';

const data = {
  title: 'Admin dashboard',
  content: 'Update course on this page'
};

export default function Create () {

  const updateCourse = (event) => {
    event.preventDefault()
    return(
      Swal.fire(
      {
        icon:"success",
        title:"Successfully Updated course!",
        text: "Thank you for updating course!"
      }
      )
      );
  };

  return(
    <div className="courseUpdate">
    <Hero bannerData={data}/>
    <Container>
    {/*<h1 className="text-center" courseUpdate >Update Course</h1>*/}
    <Form onSubmit={e => updateCourse(e)}>
  {/*Course Name Field*/}
  <Form.Group>
  <Form.Label><h6>Course Name:</h6></Form.Label>
  <Form.Control type="text" placeholder="Enter Updated Course Name" required />
  </Form.Group>

{/*Description Field*/}
<Form.Group>
<Form.Label><h6>Description: </h6>
</Form.Label>
<Form.Control type="text" placeholder="Enter Updated Description" required />
</Form.Group>


{/*Price*/}
<Form.Group>
<Form.Label><h6>Price: </h6></Form.Label>
<Form.Control type="number" placeholder="Enter Updated Price" required />
</Form.Group>

<Form>
<Form.Check
type="switch"
id="custom-switch"
label="Enable/Disable Course"
/>

</Form>
<Button variant="primary" className="btn-block" type="submit">Update
</Button>
</Form>
</Container>
</div>
);

}