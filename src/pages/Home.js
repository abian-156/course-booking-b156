//acquire all the compnents that will make up teh home page. (hero section, highlights)
import Banner from './../components/Banner';
import Highlights from './../components/Highlights';
import Footer from './../components/Footer';

//Lets create a data object the content of the hero section

const data = {
	title: 'Become a web developer',
	content: 'Join a passionate team shaping the future of IT education in the Philippines'
}	

export default function Home() {
	return(
	 <div className="banner">
		<Banner bannerData={data}/>
		<Highlights />
		<Footer />
	</div>
	);
};