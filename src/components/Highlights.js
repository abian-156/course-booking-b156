//Grid system and Card
import {Row, Col, Card, Container} from 'react-bootstrap';

//format the card with the help of utility classes of bootstrap.

export default function Highlights() {
	return(
		<Container id="card">
			<Row className="my-3">
				{/*1st Highlight*/}
				<Col xs={12} md={4}>
					<Card className="p-4 cardHighlight">
						<Card.Body>
							<Card.Title> Learn From Home </Card.Title>
							<Card.Text>
								Staying at home can be an opportunity to get your dream job in Tech, with the No. 1 Coding Bootcamp in the Philippines.
							</Card.Text>
						</Card.Body>
					</Card>			
				</Col>

			    {/*2nd Highlight*/}
			    <Col xs={12} md={4}>
			    	<Card className="p-4 cardHighlight">
			    		<Card.Body >
			    			<Card.Title>Study Now, Pay Later</Card.Title>
			    			<Card.Text>
			    				We’re confident in your ability to get your dream career after the training. Enjoy our flexible payment options.
			    			</Card.Text>
			    		</Card.Body>
			    	</Card>			
			    </Col>

		         {/*3rd Highlight*/}
		         <Col xs={12} md={4}>
		         	<Card className="p-4 cardHighlight">
		         		<Card.Body>
		         			<Card.Title> Be Part of our Community</Card.Title>
		         			<Card.Text>
		         				The Developer Career Program is inclusive of in-depth modules on static and dynamic web applications.
		         			</Card.Text>
		         		</Card.Body>
		         	</Card>			
		         </Col>
			</Row>
		</Container>
		
	)
}

