import { useState, useEffect } from 'react';
import AppNavBar from './components/AppNavBar';

//acquire the pages that will make up the app
import Home from './pages/Home';
import Register from './pages/Register';
import LoginPage from './pages/Login';
import Catalog from './pages/Courses'
import ErrorPage from './pages/Error';
import CourseView from './pages/CourseView';
import Logout from './pages/Logout';
import AddCourse  from './pages/AddCourse'
import UpdateCourse  from './pages/UpdateCourse';

//acquire the provider utility inthis entry point
import { UserProvider } from './UserContext';

//import the hero section
import './App.css';
import { BrowserRouter as Router , Routes, Route } from 'react-router-dom';

import './App.css';


function App() {

  //the role of the provider was ssisgn to the App.js which means all the information taht will decalred here will automatically becomes 'global'.
 //initialize the state of the user to privide/idenrtify the status of the client who is usung the app.
 //id -> to reference the user
 //isAdmin -> role of the user
  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });//to bind the user to the default state.


   //feed the information started here to the consumers.


   //Create a function that will unset/unmount the user
   const unsetUser = () => {
      //clear out the saved dat in the local storage
      //storaga //remove all the data in the local storage
      localStorage.clear();
      //revert the state of user back to null
      setUser({
        id: null,
        isAdmin: null
      });
   }

   //create a 'side effect' taht will send a request to API collection to identify the user. using the acces token that was saved in the browser.
   useEffect(() => {
    //identify the state of the user
      // console.log(user);
    //mount/campaign the user to the app so that he/she will be recognized.

    //retrieve the token from browser storage.
      let token = localStorage.getItem('accessToken');
      //console.log(token);
      fetch('https://thawing-gorge-69376.herokuapp.com/users/details', {
        headers: {
          authorization: `Bearer ${token}`
        }
      }).then(res => res.json()).then(convertedData => {
           // console.log(convertedData);
        //identify the procedures taht will happend if the info about the user is retrieved succesfully.
        //chnage the current state of the user.
        if (typeof convertedData._id !== "undefined") {
          setUser({
            id: convertedData._id,
            isAdmin: convertedData.isAdmin
          });
          // console.log(user)
        } else {
            //if the condition above is not met.
          setUser({
            id: null,
            isAdmin: null
          });
        }
        
      });

   },[user]);


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
         <AppNavBar />
         <Routes>
            <Route path='/' element={<Home />} /> 
            <Route path='/register' element={<Register />}  />
            <Route path='/courses' element={<Catalog />} /> 
            <Route path='*' element={<ErrorPage/>} />
            <Route path= '/login' element={<LoginPage />} />
            <Route path='/courses/view/:id' element={<CourseView/>} />
            <Route path='/logout' element={<Logout />} />
            <Route path='/add/courses' element={<AddCourse/>} />
            <Route path='/courses/update' element={<UpdateCourse/>} />
         </Routes>
      </Router>
    </UserProvider>
  );
};

export default App;
